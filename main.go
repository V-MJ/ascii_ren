package main
 
import (
	"fmt"
	"image"
	"image/draw"
	_ "image/png"
	_ "image/jpeg"
	"log"
	"os"
	"math"
	"flag"

	"golang.org/x/image/font/gofont/gomono"
	"golang.org/x/image/font/sfnt"
	"golang.org/x/image/math/fixed"
	"golang.org/x/image/vector"
)

type hashchar struct {
	hash uint64;		//identifier for this symbol
	symbol rune;		//symbol that hash refers to
	bA float64;		//brightness of symboll
}

func getLuminocity(R float64, G float64, B float64) float64 {
	return math.Sqrt( 0.299 * math.Pow(R, 2) + 0.587 * math.Pow(G, 2) + 0.114 * math.Pow(B, 2) );
}

func getAvgColor(img image.Image, sX uint64, sY uint64, lX uint64, lY uint64) (float64, float64, float64) {
	var avgR, avgG, avgB  float64 = 0, 0, 0;
	for i  := uint64(0); i  < lY; i++ {
	for ii := uint64(0); ii < lX; ii++ {
		R, G, B, _:=img.At(int(sX+ii), int(sY+i)).RGBA();
		avgR += float64(R)/257.0;
		avgG += float64(G)/257.0;
		avgB += float64(B)/257.0;
	}}
	//var avgColor color;
	avgR = avgR/float64(lX*lY);
	avgG = avgG/float64(lX*lY);
	avgB = avgB/float64(lX*lY);
	
	return avgR, avgG, avgB;
}

func getAvgBrightnessOfArea(img image.Image, sX uint64, sY uint64, lX uint64, lY uint64) float64 {
	R, G, B := getAvgColor(img, sX, sY, lX, lY);
	return getLuminocity(R, G, B);
}

func parseForShapeHash(img image.Image, sX uint64, sY uint64, lX uint64, lY uint64) uint64 {
	const sidelen uint64 = 128;
	var sArea [sidelen][sidelen]float64;	//sample area
//	var avg float64;			//average color
	for  i := uint64(0);  i < sidelen; i++{		//fill sample area
	for ii := uint64(0); ii < sidelen; ii++{
		/*calculate position to sub sample*/
		sXl := float32(sX)+float32(lX)/float32(sidelen)*float32(ii);
		sYl := float32(sY)+float32(lY)/float32(sidelen)*float32(i);
		sArea[i][ii] = float64(getAvgBrightnessOfArea(img, uint64(sXl), uint64(sYl), lX/sidelen, lY/sidelen));
	}}
	
	avg :=0.0;
	div :=0;
	for i := range sArea{
	for ii := range sArea[i]{
		avg += sArea[i][ii];
		div++;
	}}
	avg /= float64(div);
	var hash uint64 = 0;
	for i := range sArea{
		for ii := range sArea[i]{
			hash = hash << 1;
			if sArea[i][ii] >= avg{
				hash |= 1;
			}
		}
	}

	return hash;
}

func getAccurateRune(Lum float64) rune {
	var baseRuneArr = []rune{' ', '░', '▒', '▓', '█'};
	retIndx := int(math.Floor(5.0/255.0*Lum));
	if retIndx > 4{
		retIndx = 4;
	}
	return baseRuneArr[retIndx];
}

func getHashRune(hash uint64, lum float64, charList []hashchar, lumE uint64, shapeE uint64)rune{
	bestMatch := ' ';
	bestDistance := 0;
	charListI := 0;
	for h := range charList{
		//hamming distance
		hdist := 0;
		for i:=0; i<64;i++{
			if ((hash >> i) & 1) == ((charList[h].hash >> i) & 1){
				hdist++;
			}
		}
		//luminance

		if (math.Abs(charList[h].bA-lum) < math.Abs(charList[charListI].bA-lum)+float64(lumE)) && (hdist >= bestDistance-int(shapeE)) {
				bestDistance = hdist;
				bestMatch = charList[h].symbol;
		}
	}
	return bestMatch;
}

func retriveColorPair(img image.Image, sX uint64, sY uint64, lX uint64, lY uint64, hash uint64)(float64, float64, float64, float64, float64, float64){
	var fR, fG, fB float64;
	var bR, bG, bB float64;
	var fC, bC uint64;	//counters
		
	for  i := 0;  i < 8;  i++{		//fill sample area
	for ii := 0; ii < 8; ii++{
		/*calculate position to sub sample*/
		sXl := float32(sX)+float32(lX)/float32(8)*float32(ii);
		sYl := float32(sY)+float32(lY)/float32(8)*float32(i);
		tR, tG, tB :=getAvgColor(img, uint64(sXl), uint64(sYl), lX/8, lY/8);
		if ((hash>>(i*8+ii)) & 1) == 1 {
			fC++;
			fR += float64(tR);
			fG += float64(tG);
			fB += float64(tB);
		}else{
			bC++;
			bR += float64(tR);
			bG += float64(tG);
			bB += float64(tB);
		}
	}}
//	var fg, bg color;
	fR = fR/float64(fC);
	fG = fG/float64(fC);
	fB = fB/float64(fC);
	bR = bR/float64(bC);
	bG = bG/float64(bC);
	bB = bB/float64(bC);
	return fR, fG, fB, bR, bG, bB; 
}

//how many colors, what letter, 
//func printFunc(colors uint8, r rune){
//}

//func generateGlyphHash(begin rune, end rune, font font.TTF)[]hashchar{
//adapted from googles example code
func generateGlypyHash(charSet *string)[]hashchar{
	var charList []hashchar;
	//load ttf'
	ppem    := 32
	width   := 19
	height  := 36
	originX := 0
	originY := 30
	f, err := sfnt.Parse(gomono.TTF)
	if err != nil { log.Fatalf("Parse: %v", err); }

	var ru rune;
	for _, ru = range *charSet{
		//generate image
		var b sfnt.Buffer
		x, err := f.GlyphIndex(&b, ru)
		if err != nil { log.Fatalf("GlyphIndex: %v", err); }
		if x == 0 {
			continue;
		}
		segments, err := f.LoadGlyph(&b, x, fixed.I(ppem), nil)
		if err != nil { log.Fatalf("LoadGlyph: %v", err); }

		// Translate and scale that glyph as we pass it to a vector.Rasterizer.
		r := vector.NewRasterizer(width, height)
		r.DrawOp = draw.Src
		for _, seg := range segments {
			// The divisions by 64 below is because the seg.Args values have type
			// fixed.Int26_6, a 26.6 fixed point number, and 1<<6 == 64.
			switch seg.Op {
				case sfnt.SegmentOpMoveTo:
					r.MoveTo(
						float32(originX)+float32(seg.Args[0].X)/float32(64),
						float32(originY)+float32(seg.Args[0].Y)/float32(64),
					)
				case sfnt.SegmentOpLineTo:
					r.LineTo(
						float32(originX)+float32(seg.Args[0].X)/float32(64),
						float32(originY)+float32(seg.Args[0].Y)/float32(64),
					)
				case sfnt.SegmentOpQuadTo:
					r.QuadTo(
						float32(originX)+float32(seg.Args[0].X)/float32(64),
						float32(originY)+float32(seg.Args[0].Y)/float32(64),
						float32(originX)+float32(seg.Args[1].X)/float32(64),
						float32(originY)+float32(seg.Args[1].Y)/float32(64),
					)
				case sfnt.SegmentOpCubeTo:
					r.CubeTo(
						float32(originX)+float32(seg.Args[0].X)/float32(64),
						float32(originY)+float32(seg.Args[0].Y)/float32(64),
						float32(originX)+float32(seg.Args[1].X)/float32(64),
						float32(originY)+float32(seg.Args[1].Y)/float32(64),
						float32(originX)+float32(seg.Args[2].X)/float32(64),
						float32(originY)+float32(seg.Args[2].Y)/float32(64),
					)
			}
		}
			
		// Finish the rasterization: the conversion from vector graphics (shapes)
		// to raster graphics (pixels).
		dst := image.NewAlpha(image.Rect(0, 0, width, height))
		r.Draw(dst, dst.Bounds(), image.Opaque, image.Point{})




		//fill and append data
		var tmpChar hashchar;
		tmpChar.symbol = ru;
		tmpChar.hash = parseForShapeHash(dst, 0, 0, 19, 36);
		tmpChar.bA = float64(getAvgBrightnessOfArea(dst, 0, 0, 19, 36));
		charList = append(charList, tmpChar);
	}
	return charList;
}

func main() {
	userX := flag.Uint64("x", 80, "maximum X dimention");
	userY := flag.Uint64("y", 24, "maximum Y dimention");
	userF := flag.String("f", "default.png", "file to operate on")
	userOp:= flag.String("op", "simple", "operation used on image [simpe, advanced]");
	userCh:= flag.String("Ch", "_▖▗▘▙▚▛▌▔▍▕▝▞▐▏▎▋▊▉█▇▆▅▄▃▂▁▀▟▜ ", "charset for advanced output");
	userEl:= flag.Uint64("Le", 2, "luminance difference error");
	userEs:= flag.Uint64("Se", 2, "shape difference error");
	flag.Parse();

	
	var oX uint64 = *userX;
	var oY uint64 = *userY;

	catFile, err := os.Open(*userF);

	if err != nil {
		log.Fatal(err)
	}
	defer catFile.Close()
 
	imData, imType, err := image.Decode(catFile)
	if err != nil {
		fmt.Println(err)
	}

	aspectRatio := math.Max(float64(imData.Bounds().Max.X), float64(imData.Bounds().Max.Y))/math.Max(float64(imData.Bounds().Max.X), float64(imData.Bounds().Max.Y));
 	//make image fit our canvas
	if imData.Bounds().Max.X >= imData.Bounds().Max.Y {
		if oX <= oY {
			oY=oX;
		}
		oY = uint64(float64(oY)*aspectRatio);
	} else {
		if oY <= oX {
			oX=oY;
		}
		oX = uint64(float64(oX)*aspectRatio);
	}

	fmt.Println(imType, "\t", oX, oY, "\t", imData.Bounds().Max.X, imData.Bounds().Max.Y);

	if *userOp == "simple" {
		for i := uint64(0);  i < oY; i++{
			for ii := uint64(0); ii < oX; ii++{
				/*calculate position to sample*/
				sXl := float32(uint64(imData.Bounds().Max.X)/oX*ii);
				sYl := float32(uint64(imData.Bounds().Max.Y)/oY*i);
				sL := getAvgBrightnessOfArea(imData, uint64(sXl), uint64(sYl), uint64(imData.Bounds().Max.X)/oX, uint64(imData.Bounds().Max.Y)/oY);
				fmt.Printf("%c", getAccurateRune(sL));
			}
			fmt.Printf("\n");
		}
	} else {
		//generate hashes for glyphs
		glyphHash := generateGlypyHash(userCh);

		for i := uint64(0);  i < oY; i++{
			for ii := uint64(0); ii < oX; ii++{
				sXl := float32(uint64(imData.Bounds().Max.X)/oX*ii);
				sYl := float32(uint64(imData.Bounds().Max.Y)/(oY)*i);
				hash:= parseForShapeHash(imData, uint64(sXl), uint64(sYl), uint64(imData.Bounds().Max.X)/oX, uint64(imData.Bounds().Max.Y)/oY);
				lum := float64(getAvgBrightnessOfArea(imData, uint64(sXl), uint64(sYl), uint64(imData.Bounds().Max.X)/oX, uint64(imData.Bounds().Max.Y)/oY));
				fmt.Printf("%s", string(getHashRune(hash, lum, glyphHash, *userEl, *userEs)));
				//color implementation reference
				//https://stackoverflow.com/questions/4842424/list-of-ansi-color-escape-sequences
				//1bit	color	needs distinction of white on black or black on white [could be done as a global switch for how "masks" are interpreted]
				//16	color	some color lookup function with a lut... precalculated 3D array?
				//255	color	same as above
				//24bit	color	simplest as the values are just read from data
			}
			fmt.Printf("\n");
		}
	}
	
}
