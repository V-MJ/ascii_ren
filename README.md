# ascii_ren

a simple utility to conver images into a charser of your choise


## using

`go run main.go -f file.png`


## advanced use

Ascii_ren has 2 main algorithms a simple luminance to block dither character converter and a more advanced shape and luminance matching algorithm.
The simple algorithm can be called with the flag `-op simple` and advanced with `-op advanced`.
They both can take flags `-x -y` to define the x and y dimensions of the final image.
Advanced mode has flags `-Le -Se` that set a acceptable error for luminance shape and values when matching to a characters defined with the `-Ch` flag.

### simple sample

```
$ go run main.go -x 80 -y 24 -f gopher.png -op simple
png      80 24   370 300
                                        ░░░░░░░░░░░░░░                          
                               ░░▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒░  ░░░░             
                      ░▓▓▓▓▒▒▒▓▓▓████████▓▓▓▓▓▓▓▓▓████████▓▓▓▓▓▒░▒▓▓▓░          
                     ▒▓▓░ ░▓▓▓▓█▒▒▒████████▒▓▓▓▓▓░░▒████████▒▓▓▓▓░▓▓▓▒          
                      ░▒▒▒▓▓▓▓▓▓  ▒████████▒▓▓▓▓▓░░▓████████▒▓▓▓▓▓▒░            
                        ░▓▓▓▓▓▓▓▓████████▓▓▒░░░░░▓▓██████▓▓▓▓▓▓▓▓▓▓             
                        ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒░▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓░            
                        ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█▒▓█▓▓▓▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒            
                        ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒██▒██▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓            
                        ▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓            
                        ░▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓            
                         ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓            
                     ░░▒░▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓░▒▒░        
                    ▒▓▓▒░▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓░▒▓▒░       
                        ░▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓░           
                        ▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒           
                        ▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒           
                        ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒           
                        ▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓            
                        ░▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒            
                         ▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒             
                          ░▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒░              
                            ░▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒                
                          ▒▓███▒░░▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒░░  ▒██▓▓       
```

### advanced sample

```
$ go run main.go -x 80 -y 24 -f gopher.png -op advanced -Ch "█▄▐ " -Le 5 -Se 3
png      80 24   370 300
                                                                                
                                  ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐                     
                       ▐▐▐▐▐▐▐▐▐▐████████▐▐▐▐▐▐▐▐██████████▐▐▐▐▐  ▐▐▐           
                      ▐▐   ▐▐▐▐█▐ ▐████████▐▐▐▐▐▐   ████████▐▐▐▐▐ ▐▐▐▐          
                        ▐▐▐▐▐▐█▐   ████████▐▐▐▐▐▐  ▐████████▐▐▐▐▐▐              
                         ▐▐▐▐▐▐▐█████████▐▐      ▐███████▐▐▐▐▐▐▐▐▐▐             
                        ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐    ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐             
                        ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐██▐██▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐            
                        ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐██ ██▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐            
                        ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐            
                         ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐            
                         ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐            
                         ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐            
                     ▐▐  ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐ ▐▐▐        
                         ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐            
                         ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐            
                        ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐            
                        ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐            
                        ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐            
                         ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐             
                          ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐              
                           ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐               
                             ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐                 
                           ▐███▐    ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐     ▐██▐▐     

```